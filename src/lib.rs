mod error;

pub use error::Error;
use std::collections::HashMap;
use std::fmt::Formatter;

use aes_gcm_siv::{AeadInPlace, Aes256GcmSiv, Nonce};
use serde::{Deserialize, Serialize};
use strum::Display;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

pub struct IdPool {
    last: u64,
}

impl IdPool {
    pub fn new() -> Self {
        Self { last: 0 }
    }

    pub fn acquire(&mut self) -> u64 {
        self.last += 1;
        self.last
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum FormField {
    String,
    Password,
    Integer,
    Date,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Message {
    text: Option<String>,
    attachments: Option<Vec<(String, Vec<u8>)>>,
}

#[repr(u32)]
#[derive(Debug, Clone, Serialize, Deserialize, Display)]
pub enum Request {
    // Misc
    Close = 0x000_00000, // Core + CBS

    // Handshake
    HandshakeUpgradeConnection = 0x001_00000,  // CBS
    HandshakeApiVersion(String) = 0x001_00001, // Core + CBS
    HandshakeConfig = 0x001_00002,             // Core + CBS
    HandshakeSuccess = 0x001_00003,            // Core

    // Account Setup
    AccountSetupSelectOption(Vec<String>) = 0x002_00000, // CBS
    AccountSetupGetForm(HashMap<String, (FormField, bool)>) = 0x002_00001, // CBS
    AccountSetupSuccess = 0x002_00002,                   // CBS
    AccountSetupFailure(String) = 0x002_00003,           // CBS

    // Sync
    InitialSyncReady = 0x003_00000, // CBS

    // Chat
    ChatReceivedMessage(String, Message) = 0x004_00000, // CBS
    ChatReceivedEvent(String, String) = 0x004_00001,    // CBS
}

#[repr(u32)]
#[derive(Debug, Clone, Serialize, Deserialize, Display)]
pub enum Response {
    // Account Setup
    AccountSetupSelectOption(u64) = 0x002_00000, // Core
    AccountSetupFilledForm(HashMap<String, String>) = 0x002_00001, // Core

    // Standard success / neutral responses
    Success = 0xFFE_00000,       // Core + CBS
    Ackknowledged = 0xFFE_00001, // Core + CBS

    // Standard error responses
    UnexpectedRequest = 0xFFF_00000, // Core + CBS
    RessourceTimeout = 0xFFF_00001,  // Core + CBS
    PermissionDenied = 0xFFF_00002,  // Core
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Packet {
    Request { id: u64, body: Request },
    Response { id: u64, req: u64, body: Response },
}

impl std::fmt::Display for Packet {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Request { body, .. } => write!(f, "Request::{body}")?,
            Self::Response { body, .. } => write!(f, "Response::{body}")?,
        }
        Ok(())
    }
}

impl Packet {
    pub fn request(id: u64, body: Request) -> Self {
        Self::Request { id, body }
    }

    pub fn response(id: u64, req: u64, body: Response) -> Self {
        Self::Response { id, req, body }
    }

    // Serializes the packet to MessagePack and encrypts it.
    pub fn pack(&self, cipher: &Aes256GcmSiv, nonce: &Nonce) -> Result<Vec<u8>, Error> {
        let mut data =
            rmp_serde::to_vec(self).map_err(|e| Error::SerializationError(e.to_string()))?;
        cipher
            .encrypt_in_place(&nonce, b"", &mut data)
            .map_err(|_| Error::EncryptionError)?;

        let mut vec = (data.len() as u32).to_le_bytes().to_vec();
        vec.append(&mut data);

        Ok(vec)
    }

    // Deserializes a packet from encrypted MessagePack.
    pub fn unpack(data: &mut Vec<u8>, cipher: &Aes256GcmSiv, nonce: &Nonce) -> Result<Self, Error> {
        cipher
            .decrypt_in_place(&nonce, b"", data)
            .map_err(|_| Error::DecryptionError)?;
        Ok(rmp_serde::from_slice::<Packet>(data.as_slice())
            .map_err(|e| Error::DeserializationError(e.to_string()))?)
    }

    pub async fn recv<T: AsyncRead + Unpin>(
        rx: &mut T,
        cipher: &Aes256GcmSiv,
        nonce: &Nonce,
    ) -> Result<Self, Error> {
        let size = rx.read_u32_le().await.map_err(|_| Error::StreamReadError)? as usize;

        let mut buffer = vec![0u8; size];
        rx.read(&mut buffer)
            .await
            .map_err(|_| Error::StreamReadError)?;

        Packet::unpack(&mut buffer, &cipher, &nonce)
    }

    pub async fn send<T: AsyncWrite + Unpin>(
        &self,
        tx: &mut T,
        cipher: &Aes256GcmSiv,
        nonce: &Nonce,
    ) -> Result<(), Error> {
        let raw = self.pack(cipher, nonce)?;
        tx.write(raw.as_slice())
            .await
            .map_err(|_| Error::StreamWriteError)?;
        Ok(())
    }
}
