use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Failed to encrypt packet")]
    EncryptionError,

    #[error("Failed to decrypt packet")]
    DecryptionError,

    #[error("Failed to serialize packet: {0}")]
    SerializationError(String),

    #[error("Failed to deserialize packet: {0}")]
    DeserializationError(String),

    #[error("Failed to read data from the stream")]
    StreamReadError,

    #[error("Failed to write data to the stream")]
    StreamWriteError,
}
